#!/usr/bin/env python

import random
import praw

MAX_LINES = 5
SESSION = None

def get_session():
    global SESSION
    if SESSION is None:
        SESSION = praw.Reddit('cliddit')
    return SESSION

def get_hot(sub, i=None, limit=20):
    ''' Get all hot posts or i'th post '''
    subreddit = get_session().get_subreddit(sub)
    if i is not None:
        hot_gen = subreddit.get_hot(limit=i)
        return [next(hot_gen) for _ in range(i+1)][-1]
    else:
        hot_gen = subreddit.get_hot(limit=limit)
        return list(hot_gen)

def get_random_hot(subs, limit=20):
    if isinstance(subs, basestring):
        sub = subs
    else:
        sub = random.choice(subs)
    return random.choice(get_hot(sub, limit=20))

def summary(selftext, max_lines=MAX_LINES, first_indent=0, indent=0):
    lines = []
    for i, line in enumerate(selftext.splitlines()):
        if i == 0:
            right = 80 - first_indent
        else:
            right = 80 - indent
        while line:
            lines += [line[:right]]
            line = line[right:]
    return '\n'.join(lines[:max_lines])

def format_post(post, max_lines=MAX_LINES):
    if post.selftext:
        text = summary(post.selftext, max_lines=max_lines)
    else:
        text = post.url
        if post.comments:
            comment = post.comments[0]
            text += '\n'
            start = '%s: ' % comment.author.name
            text += '%s%s' % (
                start,
                summary(comment.body, max_lines=3, first_indent=len(start))
            )
    s = '''%s -- %s\n%s\n''' % (post.title, post.author.name, text)
    return s

if __name__ == '__main__':
    print(format_post(get_random_hot(('python', 'funny', 'wtf'))))
